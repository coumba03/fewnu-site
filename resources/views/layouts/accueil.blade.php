@extends('base')

@section('content')

<div class="header-content container-fluid py-5 mb-5 ">
    <div class="row container  pb-5">
        <div class="col-md-6 col-sm-12 col-xs-12">
            <p class="para-un text-white py-3 my-4">Decouvrez votre suite de logiciel de gestion fewnu</p>
            <div>
                <p class=" para-b bg-white py-3 my-3 w-75 text-center">Téléchargez sur pc</p>
            </div>
            <div class="foto-net mt-4">
                <img src="images/google.png" alt="" class="mt-2">
                <img src="images/store.png" alt="" class="mt-2">
            </div>
        </div>
        <div class="col-md-6 col-sm-12 col-xs-12">
            <img src="images/computer.png" alt="" class="w-100 mt-5 pt-4">
        </div>
    </div>
</div>
<div class="middle-content container mb-5">
    <div class="row  ">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="text-center">
                <img src="images/Management.png" alt="" class="">
                <h3 class="text-uppercase">Gestion</h3>
            </div>           
            <p>Faciliter de bien gérer l’activité économique <br> d’une entreprise</p>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="text-center">
                <img src="images/Union.png" alt="" class="">
                <h3 class="text-uppercase">Facturation</h3>
            </div>
            <p>Etablir une note détaillée des prestations <br> ou marchandise  vendues</p>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="text-center">
                <img src="images/tayeur.png" alt="" class="">
                <h3 class="text-uppercase">Tayeur</h3>
            </div>
            <p>Etablir une note détaillée des prestations <br> ou marchandise  vendues</p>
        </div>
    </div>
</div>
<div class="last-section py-5 mb-5 position-relative">
    <h2 class="text-white para-last-sec mx-5 py-5">GEREZ VOTRE <br> ENTREPRISE AVEC FEWNU</h2>
    <div class="telec text-white  mx-5 pb-5 ">
        <p class="para-telec text-center w-25">Téléchargez</p>
    </div>
</div>

@endsection