<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title', env('APP_NAME'))</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
        
        <link rel="stylesheet" href="css/css.css">

       
    </head>
    <body class="overflo-hidden">

        <nav class="navbar navbar-expand-lg bg-white shadow pt-0 ">
            <div class="container-fluid">
                <a class="navbar-brand" href="/accueil">
                    <img src="images/fewnu.png" alt="fewnu" class="w-75">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse mx-5 " id="navbarNav">
                    <ul class="navbar-nav  ms-auto">
                       <li class="nav-item me-5 ">
                          <a class="nav-link active" aria-current="page" href="/accueil">Accueil</a>
                       </li>
                       <li class="nav-item me-5">
                          <a class="nav-link" href="/a-propos">A propos</a>
                       </li>
                       <li class="nav-item me-5">
                          <a class="nav-link" href="/fonctionnalite">Fonctionnalités</a>
                       </li>
                       <li class="nav-item me-5">
                          <a class="nav-link" href="/download">Télécharger</a>
                       </li>
                       <li class="nav-item me-5 ps-5 ">
                          <a class="nav-link " href="/contact"> <button class="btn btn-default btn-contact">Contacts</button></a>
                       </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="">
            @yield('content')
        </div>

        <footer class="container-fluid py-4">
            <div class="row">
                <div class="col-md-9 col-sm-12 col-xs-12 d-flex ">
                    <div class="fewnu-b">
                        <img src="images/fewnu.png" alt="" class="">
                    </div>
                    <div class="text-disp">
                        <p>Nous vous coachons et vous aidons à atteindre vos objectifs pour votre <br> Business. Avec Fewnu, nous pouvons vous aider à chaque étape du processus. <br> Téléchargez votre Logiciel de Gestion Fewnu et facilitez votre quotidien. <br> Qu’est-ce que vous attendez pour faire le premier pas aujourd’hui ?
                        </p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <img src="images/fcb.png" alt="" class="">
                    <img src="images/insta.png" alt="" class="ms-3">
                    <img src="images/twitt.png" alt="" class="ms-3">
                </div>
            </div>
            <hr>
            <div>
                <p>Nous sommes disponibles!</p>
            </div>
            <div class="foot row">
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <h5>CONTACTEZ - NOUS</h5>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <p> 
                        <span><img src="images/fone.png" alt="" class="foot-contact"></span>
                         78 293 36 56
                    </p>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12">
                    <p> 
                        <span><img src="images/mail.png" alt="" class="foot-contact"></span>
                        fewnu2022@gmail.com
                    </p>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <p>
                        <span><img src="images/location.png" alt="" class="foot-contact"></span>
                        Hlm Grand Yoff
                    </p>
                </div>
                <div class="col-md-2 col-sm-12 col-xs-12">
                    <p>
                        <span><img src="images/timer.png" alt="" class="foot-contact"></span>
                        8:00 - 18:00
                    </p>
                </div>
            </div>
            <div class="copyri text-center">
                <p>&copy; Copyright Fewnu – Logiciel {{date('Y')}} Tous Droits Réservés
                </p>
            </div>
        </footer>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
    </body>
</html>
